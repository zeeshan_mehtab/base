<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = new Role();
        $superAdmin->name = 'super-admin';
        $superAdmin->display_name = 'Super Admin';
        $superAdmin->description = 'Users with this role have global permissions for all actions.';
        $superAdmin->save();

        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'Admin';
        $admin->description = 'Users with this role can access time logs of employees and they can also manage employees.';
        $admin->save();

        $agent = new Role();
        $agent->name = 'employee';
        $agent->display_name = 'Employee';
        $agent->description = 'Users with this role can view their own time logs and create new.';
        $agent->save();
    }
}
