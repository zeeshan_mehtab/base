<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		$superAdmin = Role::find(1);
		$admin = Role::find(2);
		$employee = Role::find(3);    		

        $user = new User();
        $user->name = 'Super User';
        $user->email = 'superadmin@localhost.com';
        $user->password = Hash::make('123456');
        $user->confirmed = 1;
        $user->save();

        $user->attachRole($superAdmin);
        $user->attachRole($admin);

        $user2 = new User();
        $user2->name = 'Admin User';
        $user2->email = 'admin@localhost.com';
        $user2->password = Hash::make('123456');
        $user2->confirmed = 1;
        $user2->save();

        $user2->attachRole($admin);

        $user3 = new User();
        $user3->name = 'Employee User';
        $user3->email = 'employee@localhost.com';
        $user3->password = Hash::make('123456');
        $user3->confirmed = 1;
        $user3->save();

        $user3->attachRole($employee);
    }
}
