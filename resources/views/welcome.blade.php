@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1 text-center">
        <div class="panel panel-default">
            <div class="panel-body">
            	<h2>Welcome to Employee attendance system<h2>
            	<p>Please login to register your time logs</p>
            	<p><a class="btn btn-primary" href="{{ url('/') }}/login">Login</a></p>
            </div>
        </div>
    </div>
</div>
@endsection
