@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Current Status</div>
            <div class="panel-body">
                @if(count($activeLogs) > 0) 
                    <p>You checked in at {{$activeLogs[0]->start_datetime}}</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('checkout') }}">
                    <input type="hidden" name="id" value="{{$activeLogs[0]->id}}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">
                        Check Out
                    </button>
                    </form>
                @else
                    <p>No active time log found</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('checkin') }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">
                        Check In
                    </button>
                    </form>
                @endif
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Time logs</div>

            <div class="panel-body">
                <form class="form-horizontal" role="form" method="GET" action="{{ route('home') }}">
                    <div class="row">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="col-lg-4 col-md-4 col-sm-6 margin-bottom-10">
                                        <select name="user" class="form-control">
                                            <option value="">Select Employee</option>
                                            @foreach ($employees as $employee)
                                            <option value="{{ $employee->id}}">{{ $employee->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="row">
                                        <div class="col-lg-12 margin-bottom-10">
                                            <button type="submit" class="btn btn-primary">
                                                Filter Time Logs
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Log #</th>
                                    <th>User Name</th>
                                    <th>Start Date/Time</th>
                                    <th>End Date/Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($logs) > 0)
                                @foreach ($logs as $log)
                                <tr>
                                    <td>{{ $log->id}}</td>
                                    <td>{{ $log->user->name}}</td>
                                    <td>{{ $log->start_datetime}}</td>
                                    <td>{{ $log->end_datetime}}</td>
                                </tr>
                                @endforeach
                                @else
                                    <tr><td colspan="4">No time logs found</td></tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <?php echo $logs->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
