<?php

namespace App\Repositories;

/*
 * Generic class for DbRepository
 */

use App\Repositories\Traits\FilterableTrait;

class DbRepository {

    use FilterableTrait;

    public function filterByAll($filters) {
        foreach ($filters as $field => $value) {
            $this->addFilter($field, $value);
        }
        return $this;
    }

    public function filterBy($field, $value) {
        $this->addFilter($field, $value);
        return $this;
    }

    public function orderBy($field, $value = "ASC") {
        $this->query->orderBy($field, $value);
        return $this;
    }

    public function filterByName($value) {
        $this->query()->where('name', 'LIKE', "%$value%");
    }

}
