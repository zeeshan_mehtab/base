<?php

namespace App\Repositories\Contracts;

/*
 * Parent Interface for all the repository interfaces.
 */

interface RepositoryInterface {

    public function all();

    public function find($id);

    public function paginate($pagesize);
}
