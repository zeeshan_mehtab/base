<?php

namespace App\Repositories;

/*
 * Repository to fetch ingredients from DB
 */

use App\Repositories\Contracts\TimelogRepositoryInterface;
use App\Models\Timelog;
use App\Repositories\DbRepository;
use Illuminate\Support\Facades\DB;

class DbTimelogRepository extends DbRepository implements TimelogRepositoryInterface {

    public function __construct() {
        $this->query = Timelog::query();
        $this->validFilterableFields = ['user', 'name', 'active'];
    }
    
    public function find($id) {
        return Timelog::findOrFail($id);
    }

    public function all() {
        return $this->applyFiltersToQuery($this->query())->get();
    }

    public function paginate($pagesize) {
        return $this->applyFiltersToQuery($this->query())->paginate($pagesize);
    }

    public function filterByUser($value) {
        $this->query()->where('timelogs.user_id', '=', "$value");
    }
    
    public function filterByActive($value) {
        $this->query()->whereNull('timelogs.end_datetime');
    }

}
