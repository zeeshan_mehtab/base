<?php

namespace App\Repositories\Traits;

trait FilterableTrait {

    protected $validFilterableFields = [];
    protected $filters = [];
    protected $query;
    
    protected function addFilter($field, $value) {
        if (!in_array($field, $this->validFilterableFields)) {
            return false;
        }
        if(empty($value)) {
            return false;
        }
        $filterMethod = 'filterBy' . camel_case($field);
        if (method_exists($this, $filterMethod)) {
            $this->$filterMethod($value);
        } else {
            $this->filters[$field] = $value;
        }
        return true;
    }

    protected function applyFiltersToQuery($query) {
        foreach ($this->filters as $field => $value) {
            $query->where($field, $value);
        }
        return $query;
    }

    protected function query() {
        return $this->query;
    }

}
