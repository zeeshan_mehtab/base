<?php

namespace App\Models;

/*
 * This file is part of News Stand Project.
 * Copyright (c) 2015 Zeeshan Mehtab.
 * This file is open source and can be used and redistributed
 */

/**
 * Description of Role
 *
 * @author Zeeshan
 */
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

}
