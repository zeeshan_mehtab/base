<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register Repositories.
     *
     * This service provider is a place to register different repositories.
     *
     * @return void
     */
    public function register() {
       
        
        $this->app->bind(
                        'App\Repositories\Contracts\TimelogRepositoryInterface', 
                        'App\Repositories\DbTimelogRepository'
                    );
    }

}
