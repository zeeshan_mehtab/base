<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\TimelogRepositoryInterface;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Models\Timelog;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $timelogsRepo;

    public function __construct(TimelogRepositoryInterface $repository)
    {
        $this->middleware('auth');
        $this->timelogsRepo = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $filters = Input::all();
        
        $user = Auth::user();
        if ($user && $user->hasRole('employee')) {
            $filters["user"] = Auth::user()->id;
            $employees = [];
        } else {
            $employees = User::with(['roles' => function($q){
                $q->where('name', 'employee');
            }])->get();
        }    

        $logs = $this->timelogsRepo->filterByAll($filters)->orderBy('start_datetime', 'DESC')->paginate($this->pagesize)->appends($filters);

        $activeLogs = $this->timelogsRepo->filterByAll(['active' => true])->all();

        return view('home')
                        ->with('filters', $filters)
                        ->with('activeLogs', $activeLogs)    
                        ->with('employees', $employees)    
                        ->with('logs', $logs);
    }

    public function checkin()
    {

        $log = new Timelog();
        $log->start_datetime = Carbon::now();    
        $log->user_id = Auth::user()->id;
        $log->save();

        return redirect("home")->with(['success' => 'Successfully checked in.']);
    }

    public function checkout()
    {
        $input = Input::all();
        $log = Timelog::findOrFail($input["id"]);
        $log->end_datetime = Carbon::now();
        $log->save();

        return redirect("home")->with(['success' => 'Successfully checked out.']);
    }
}
